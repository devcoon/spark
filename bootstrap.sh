#!/bin/bash

# -  
# - setting spark defaults
# -
echo spark.yarn.jar hdfs://spark/spark-assembly-"${SPARK_VERSION}"-hadoop"${HADOOP_VERSION}".jar > "${SPARK_HOME}"/conf/spark-defaults.conf
echo spark.eventLog.enabled   true >> "${SPARK_HOME}"/conf/spark-defaults.conf
echo spark.eventLog.dir       file:///var/log/  >> "${SPARK_HOME}"/conf/spark-defaults.conf
echo spark.eventLog.compress  true >> "${SPARK_HOME}"/conf/spark-defaults.conf
#echo spark.history.fs.logDirectory   /path/of/history/log/directory >> "${SPARK_HOME}"/conf/spark-defaults.conf

cp "${SPARK_HOME}"/conf/metrics.properties.template "${SPARK_HOME}"/conf/metrics.properties

#Start Master and Worker
."${SPARK_HOME}"/sbin/start-master.sh #-i "${SPARK_LOCAL_IP}"
."${SPARK_HOME}"/sbin/start-slave.sh spark://"${SPARK_MASTER_IP}":"${SPARK_MASTER_PORT}" #-i "${SPARK_LOCAL_IP}"

while :; do
 sleep 300
done